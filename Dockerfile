FROM registry.gitlab.com/bsu/cme-lab/images/base-image:latest

# Install our software stack
RUN apt update && \ 
    apt install --no-install-recommends -y git curl && \
    apt clean &&\
    rm -rf /var/lib/apt/lists/* && \ 
    conda install -y --only-deps -c omnia -c conda-forge -c mosdef foyer mbuild python=3.6 && \
    conda install -y -c ambermd -c openbabel ambertools=19 openbabel && \
    conda clean -tipsy && \
    pip install --no-cache-dir --upgrade git+https://bitbucket.org/cmelab/cme_utils.git git+https://github.com/mosdef-hub/foyer.git git+https://github.com/mikemhenry/mbuild.git@mike/plankton signac signac-flow
